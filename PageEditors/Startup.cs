﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PageEditors.Data.Repository;
using PageEditors.Data.Repository.PageContentsRepository.HomePageTextContentRepository;
using PageEditors.Data.Repository.PageContentsRepository.SupportUsPageRepository;
using PageEditors.Services;
using PageEditors.Services.HomePageService;
using PageEditors.Services.SupportUsService;

namespace PageEditors
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSingleton<IAboutUsContentRepository, AboutUsContentRepository>();
            services.AddSingleton<IHomePageContentRepository, HomePageContentRepository>();
            services.AddSingleton<ISupportUsContentRepository, SupportUsContentRepository>();


            services.AddTransient<IAboutUsContentService, AboutUsContentService>();
            services.AddTransient<IHomePageContentService, HomePageContentService>();
            services.AddTransient<ISupportUsService, SupportUsService>();
            services.AddTransient<IPairService, PairService>();

            services.AddCors(c =>
            {
                c.AddPolicy("AllowOrigin", options => { options.AllowAnyOrigin(); options.AllowAnyMethod(); options.AllowAnyHeader(); });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseCors(options =>
            {
                options.AllowAnyOrigin();
                options.AllowAnyMethod();
                options.AllowAnyHeader();
            });

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
