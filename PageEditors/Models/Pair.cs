﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PageEditors.Models
{
    public class Pair
    {
        public string Code { get; set; }
        public string Resource { get; set; }
    }
}
