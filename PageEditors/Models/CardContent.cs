﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace PageEditors.Models
{
    public class CardContent
    {
        public string Code { get; set; }
        [MaxLength(30, ErrorMessage = "Capacity Overflowed")]
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public string BackgroundColor{ get; set; }
    }
}
