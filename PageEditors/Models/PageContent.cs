﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PageEditors.Models
{
    public class PageContent
    {
        [Required]
        public string Code { get; set; } //Works as the ID
        [MaxLength(50, ErrorMessage ="Capacity Overflowed")]
        public string Title { get; set; }
        [MaxLength(250, ErrorMessage ="Capacity Overflowed")]
        public string Content { get; set; }
        public string ImageURL { get; set; }
    }
}
