﻿using PageEditors.Data.Repository.PageContentsRepository.SupportUsPageRepository;
using PageEditors.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PageEditors.Services.SupportUsService
{
    public class PairService : IPairService
    {
        private ISupportUsContentRepository repository;
        public PairService(ISupportUsContentRepository repository)
        {
            this.repository = repository;
        }
        public Pair addPairToList(Pair pair)
        {
            return repository.addPairToResource(pair);
        }

        public bool deletePair(string code, string resource)
        {
            if (getPair(code, resource) == null)
                throw new Exception("Not found");
            return repository.deletePair(code, resource);
        }

        public Pair getPair(string code, string resource)
        {
            return repository.getPair(code, resource);
        }

        public Pair updatePairOf(string code, string resource, Pair pair)
        {
            if (repository.getPair(code, resource) == null)
                throw new Exception("Not found");
            if (pair.Resource == null)
                pair.Resource = resource;
            return repository.updatePairOfResource(code, resource, pair);
        }
    }
}
