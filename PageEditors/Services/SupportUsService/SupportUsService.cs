﻿using PageEditors.Data.Repository.PageContentsRepository.SupportUsPageRepository;
using PageEditors.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PageEditors.Services.SupportUsService
{
    public class SupportUsService : ISupportUsService
    {
        private ISupportUsContentRepository repository;
        public SupportUsService(ISupportUsContentRepository repository)
        {
            this.repository = repository;
        }
        public ResourceContent GetResource(string code)
        {
            return repository.getSpecificResource(code);
        }

        public IEnumerable<ResourceContent> GetResourceContents()
        {
            return repository.getAllResources();
        }

        public ResourceContent UpdateResource(string code, ResourceContent resource)
        {
            if (GetResource(code) == null)
                throw new Exception("Error in DB");
            if (resource.Title == null)
                resource.Title = GetResource(code).Title;
            if (resource.Description == null)
                resource.Description = GetResource(code).Description;
            if (resource.ImageUrl == null)
                resource.ImageUrl = GetResource(code).ImageUrl;
            return repository.updateResource(code, resource);
        }
    }
}
