﻿using PageEditors.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PageEditors.Services.SupportUsService
{
    public interface ISupportUsService
    {
        IEnumerable<ResourceContent> GetResourceContents();
        ResourceContent GetResource(string code);
        ResourceContent UpdateResource(string code, ResourceContent resource);
    }
}
