﻿using PageEditors.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PageEditors.Services.SupportUsService
{
    public interface IPairService
    {
        Pair getPair(string code, string resource);
        Pair addPairToList(Pair pair);
        Pair updatePairOf(string code, string resource, Pair pair);
        bool deletePair(string code, string resource);

    }
}
