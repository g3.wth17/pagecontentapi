﻿using PageEditors.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PageEditors.Services.HomePageService
{
    public interface IHomePageContentService
    {
        IEnumerable<PageContent> getAllTextContent();
        PageContent getSpecificTextContent(string code);
        PageContent updateTextcontent(string code, PageContent page);
        IEnumerable<CardContent> getAllCards();
        CardContent getSpecificCard(string code);
        CardContent updateCardContent(string code, CardContent card);
    }
}
