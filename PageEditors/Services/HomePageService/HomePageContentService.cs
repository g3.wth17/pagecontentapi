﻿using PageEditors.Data.Repository.PageContentsRepository.HomePageTextContentRepository;
using PageEditors.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PageEditors.Services.HomePageService
{
    public class HomePageContentService : IHomePageContentService
    {
        private IHomePageContentRepository repository;
        public HomePageContentService(IHomePageContentRepository repository)
        {
            this.repository = repository;
        }

        public IEnumerable<CardContent> getAllCards()
        {
            return repository.getAllCards();
        }

        public IEnumerable<PageContent> getAllTextContent()
        {
            return repository.getTextPageContent();
        }

        public CardContent getSpecificCard(string code)
        {
            return repository.getSpecificCard(code);
        }

        public PageContent getSpecificTextContent(string code)
        {
            return repository.getSpecificiTextContent(code);
        }

        public CardContent updateCardContent(string code, CardContent card)
        {
            if (getSpecificCard(code) == null)
                throw new Exception("Content not found");
            if (card.ImageUrl == null)
                card.ImageUrl = getSpecificCard(code).ImageUrl;
            if (card.BackgroundColor == null)
                card.BackgroundColor = getSpecificCard(code).BackgroundColor;
            return repository.updateCardContent(code, card);
        }

        public PageContent updateTextcontent(string code, PageContent page)
        {
            if (getSpecificTextContent(code) == null)
                throw new Exception("Content not found");
            if (page.Content == null)
                page.Content = getSpecificTextContent(code).Content;
            return repository.updateTextContent(code, page);
        }
    }
}
