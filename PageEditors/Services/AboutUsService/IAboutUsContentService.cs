﻿using PageEditors.Data.Repository;
using PageEditors.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PageEditors.Services
{
    public interface IAboutUsContentService
    {
        IEnumerable<PageContent> getAllAboutUsContent();
        PageContent getPageContentByTitle(string code);
        PageContent updateContent(string code, PageContent page);
    }
}
