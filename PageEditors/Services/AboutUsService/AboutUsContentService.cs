﻿using PageEditors.Data.Repository;
using PageEditors.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PageEditors.Services
{
    public class AboutUsContentService : IAboutUsContentService
    {
        private IAboutUsContentRepository repository;
        public AboutUsContentService(IAboutUsContentRepository repository)
        {
            this.repository = repository;
        }
        public IEnumerable<PageContent> getAllAboutUsContent()
        {
            return repository.getAllContent();
        }

        public PageContent getPageContentByTitle(string code)
        {
            return repository.getSpecificContent(code);
        }

        public PageContent updateContent(string code, PageContent page)
        {
            if (getPageContentByTitle(code) == null)
                throw new Exception("Content not found");
            if (page.Title == null)
                page.Title = getPageContentByTitle(code).Title;
            if (page.Content == null)
                page.Content = getPageContentByTitle(code).Content;
            return repository.updateContent(code, page);
        }
    }
}
