﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PageEditors.Models;
using PageEditors.Services;


namespace PageEditors.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AboutUsController : ControllerBase
    {
        private IAboutUsContentService service;
        public AboutUsController(IAboutUsContentService service)
        {
            this.service = service;
        }
        [HttpGet]
        //Get all content
        public ActionResult<IEnumerable<PageContent>> GetAllContent()
        {
            try
            {
                return Ok(service.getAllAboutUsContent());
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("{code}")]
        public ActionResult<PageContent> getSpecificContent(string code)
        {
            try
            {
                return Ok(service.getPageContentByTitle(code));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpPut("{code}")]
        public ActionResult<PageContent> updateSpecificContent(string code, [FromBody] PageContent page)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();
                return Ok(service.updateContent(code, page));
            }
            catch (Exception)
            {

                return BadRequest();
            }
        }

    }
}
