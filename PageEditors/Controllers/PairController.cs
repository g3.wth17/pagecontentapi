﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PageEditors.Models;
using PageEditors.Services.SupportUsService;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PageEditors.Controllers
{
    [Route("api/SupportUs/{code:minLength(4)}/Pair")]
    [ApiController]
    public class PairController : ControllerBase
    {
        private IPairService service;
        public PairController(IPairService service)
        {
            this.service = service;
        }

        [HttpPost]
        public ActionResult<Pair> postPair([FromBody] Pair pair)
        {
            try
            {
                return Ok(service.addPairToList(pair));
            }
            catch (Exception)
            {

                return BadRequest();
            }
        }

        [HttpPut("{resource}")]
        public ActionResult<Pair> PutPair(string code, string resource, [FromBody] Pair pair)
        {
            try
            {
                return Ok(service.updatePairOf(code, resource, pair));
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{resource}")]
        public ActionResult<bool> DeletePair(string code, string resource)
        {
            try
            {
                return Ok(service.deletePair(code, resource));
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
