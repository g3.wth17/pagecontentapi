﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PageEditors.Models;
using PageEditors.Services.SupportUsService;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PageEditors.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SupportUsController : Controller
    {
        private ISupportUsService service;
        public SupportUsController(ISupportUsService service)
        {
            this.service = service;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ResourceContent>> GetAllResorces()
        {
            try
            {
                return Ok(service.GetResourceContents());
            }
            catch (Exception)
            {
                return BadRequest();
                
            }
        }

        [HttpGet("{code}")]
        public ActionResult<ResourceContent> GetResource(string code)
        {
            try
            {
                return Ok(service.GetResource(code));
            }
            catch (Exception)
            {
                return NotFound();
                throw;
            }
        }
        [HttpPut("{code}")]
        public ActionResult<ResourceContent> PutResource(string code, [FromBody] ResourceContent resource)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();
                return Ok(service.UpdateResource(code, resource));
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

    }
}
