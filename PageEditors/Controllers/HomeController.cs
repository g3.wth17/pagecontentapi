﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PageEditors.Models;
using PageEditors.Services.HomePageService;

namespace PageEditors.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private IHomePageContentService service;
        public HomeController(IHomePageContentService service)
        {
            this.service = service;
        }
        [HttpGet("Text")]
        public ActionResult<IEnumerable<PageContent>> GetAllTextContent()
        {
            try
            {
                return Ok(service.getAllTextContent());
            }
            catch (Exception)
            {

                return BadRequest();
            }
        }
        [HttpGet("Cards")]
        public ActionResult<IEnumerable<CardContent>> GetAllCards()
        {
            try
            {
                return Ok(service.getAllCards());
            }
            catch (Exception)
            {

                return BadRequest();
            }
        }

        [HttpGet("Text/{code}")]
        public ActionResult<PageContent> GetSpecificTextContent(string code)
        {
            try
            {
                return Ok(service.getSpecificTextContent(code));
            }
            catch (Exception)
            {

                return NotFound();
            }
        }
        
        [HttpGet("Cards/{code}")]
        public ActionResult<CardContent> GetSpecificCard(string code)
        {
            try
            {
                return Ok(service.getSpecificCard(code));
            }
            catch (Exception)
            {

                return NotFound();
            }
        }

        [HttpPut("Text/{code}")]
        public ActionResult<PageContent> UpdateSpecificTextContent(string code, [FromBody] PageContent page)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();
                return Ok(service.updateTextcontent(code, page));
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPut("Cards/{code}")]
        public ActionResult<CardContent> UpdateSpecificCard(string code, [FromBody] CardContent card)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();
                return Ok(service.updateCardContent(code, card));
            }
            catch (Exception)
            {

                return BadRequest();
            }
        }
    }
}
