﻿using PageEditors.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace PageEditors.Data.Repository.PageContentsRepository.HomePageTextContentRepository
{
    public class HomePageContentRepository : IHomePageContentRepository
    {
        List<PageContent> textPageContent;
        List<CardContent> cardsPageContent;
        public HomePageContentRepository()
        {
            textPageContent = new List<PageContent>()
            {
                new PageContent()
                {
                     Code= "PreguntaPaginaInicial",
                     Title="Pregunta",
                     Content="Te imaginas desarrollar una empresa que te permita vivir haciendo lo que te apasiona al mismo tiempo que generas impacto positivo en el mundo?",
                     ImageUrl= ""
                },
                new PageContent()
                {
                     Code= "DescrpcionOrganizacionPaginaInicial",
                     Title="Descripcion",
                     Content="En Clubes de Innovación y Emprendimiento trabajamos para poner a tu alcance conocimiento y a personas dispuestas a apoyarte para que desarrolles una idea de negocio que te permita mejorar nuestro mundo de forma sostenible",
                     ImageUrl= ""
                }
            };

            cardsPageContent = new List<CardContent>()
            {
                new CardContent()
                {
                     Code = "ComunidadClubes",
                     Title = "Clubes",
                     ImageUrl = "clubes.img",
                     BackgroundColor = "#FFF0F8FF"
                },
                new CardContent()
                {
                     Code = "ComunidadMiembros",
                     Title = "Miembros",
                     ImageUrl = "clubes.img",
                     BackgroundColor = "#FFF0F8FF"
                },
                new CardContent()
                {
                     Code = "ComunidadMentores",
                     Title = "Mentores",
                     ImageUrl = "clubes.img",
                     BackgroundColor = "#FFF0F8FF"
                },
                new CardContent()
                {
                     Code = "ComunidadAsesores",
                     Title = "Asesores",
                     ImageUrl = "clubes.img",
                     BackgroundColor = "#FFF0F8FF"
                },
                new CardContent()
                {
                     Code = "ComunidadFacilitadores",
                     Title = "Facilitadores",
                     ImageUrl = "clubes.img",
                     BackgroundColor = "#FFF0F8FF"
                },
                new CardContent()
                {
                     Code = "ComunidadAliadosEstrategicos",
                     Title = "Aliados Estrategicos",
                     ImageUrl = "https://images.app.goo.gl/Gm9xDLPizkdvhm599",
                     BackgroundColor = "#FFF0F8FF"
                }
            };
        }

        public IEnumerable<CardContent> getAllCards()
        {
            return cardsPageContent;
        }

        public CardContent getSpecificCard(string code)
        {
            return cardsPageContent.FirstOrDefault(c => c.Code == code);
        }

        public PageContent getSpecificiTextContent(string code)
        {
            return textPageContent.FirstOrDefault(p => p.Code == code);
        }

        public IEnumerable<PageContent> getTextPageContent()
        {
            return textPageContent;
        }

        public CardContent updateCardContent(string code, CardContent card)
        {
            var cardToUpdate = cardsPageContent.FirstOrDefault(c => c.Code == code);
            cardToUpdate.Title = card.Title;
            cardToUpdate.ImageUrl = card.ImageUrl;
            cardToUpdate.BackgroundColor = card.BackgroundColor;
            return cardToUpdate;
        }

        public PageContent updateTextContent(string code, PageContent page)
        {
            var contentToUpdate = textPageContent.FirstOrDefault(p => p.Code == code);
            contentToUpdate.Content = page.Content;
            contentToUpdate.ImageUrl = page.ImageUrl;
            return contentToUpdate;
        }
    }
}
