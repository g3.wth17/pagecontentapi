﻿using PageEditors.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PageEditors.Data.Repository.PageContentsRepository.HomePageTextContentRepository
{
    public interface IHomePageContentRepository
    {
        IEnumerable<PageContent> getTextPageContent();
        PageContent getSpecificiTextContent(string code);
        PageContent updateTextContent(string code, PageContent page);
        IEnumerable<CardContent> getAllCards();
        CardContent getSpecificCard(string code);

        CardContent updateCardContent(string code, CardContent card);
    }
}
