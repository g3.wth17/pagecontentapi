﻿using PageEditors.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PageEditors.Data.Repository
{
    public class AboutUsContentRepository : IAboutUsContentRepository
    {
        List<PageContent> aboutUsContent;
        public AboutUsContentRepository()
        {
            aboutUsContent = new List<PageContent>()
            {
                new PageContent(){
                     Code="QuienesSomos",
                     Title = "Quienes Somos",
                     Content = "Una Bolivia líder y autosuficiente cuyos habitantes vivan felices y en paz aportando a su desarrollo y al del mundo.",
                     ImageUrl = ""
                },
                new PageContent(){
                     Code="Vision",
                     Title = "Vision",
                     Content = "Ser la principal red mundial de gestores de ciencia, tecnología, innovación y emprendimiento socialmente apropiados.",
                     ImageUrl = ""
                },
                new PageContent(){
                     Code="Mision",
                     Title = "Mision",
                     Content = "Contribuimos en la educación de ciudadanos activos, empoderados, comprometidos y competentes para intervenir positivamente en su realidad por medio de la formación temprana en ciencia, tecnología, innovación y emprendimiento socialmente apropiados.",
                     ImageUrl = ""
                },
                new PageContent(){
                     Code="Objetivos",
                     Title = "Objetivos",
                     Content = "Sensibilizar sobre la importancia y alcances de la Ciencia, la Tecnología, la Innovación y Emprendimiento Socialmente Apropiados.\n " +
                                "Desarrollar y fortalecer en niñas, niños y jóvenes, competencias relacionadas al quehacer científico-tecnológico, a la innovación y al emprendimiento socialmente apropiados.\n " +
                                "Promover el desarrollo de ciencia, tecnología, innovación y emprendimiento que contribuya al desarrollo social, ambiental, productivo y económico de Bolivia y el mundo.",
                     ImageUrl = ""
                }
            };
        }

        public PageContent updateContent(string code, PageContent page)
        {
            var pageToUpdate = aboutUsContent.FirstOrDefault(p => p.Code == code);
            pageToUpdate.Title = page.Title;
            pageToUpdate.Content = page.Content;
            pageToUpdate.ImageUrl = page.ImageUrl;
            return pageToUpdate;
        }

        public IEnumerable<PageContent> getAllContent()
        {
            return aboutUsContent;
        }

        public PageContent getSpecificContent(string code)
        {
            return aboutUsContent.FirstOrDefault(p => p.Code == code);
        }
    }
}
