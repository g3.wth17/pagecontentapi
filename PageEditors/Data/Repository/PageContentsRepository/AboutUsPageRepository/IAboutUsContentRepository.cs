﻿
using PageEditors.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PageEditors.Data.Repository
{
    public interface IAboutUsContentRepository
    {
        IEnumerable<PageContent> getAllContent();

        PageContent getSpecificContent(string code);

        PageContent updateContent(string code, PageContent page);

    }
}
