﻿using PageEditors.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PageEditors.Data.Repository.PageContentsRepository.SupportUsPageRepository
{
    public interface ISupportUsContentRepository
    {
        IEnumerable<ResourceContent> getAllResources();
        ResourceContent getSpecificResource(string code);
        ResourceContent updateResource(string code, ResourceContent resource);
        Pair getPair(string code, string resource);
        Pair addPairToResource(Pair pair);
        Pair updatePairOfResource(string code, string resource, Pair pair);
        bool deletePair(string code, string resource);
    }
}
