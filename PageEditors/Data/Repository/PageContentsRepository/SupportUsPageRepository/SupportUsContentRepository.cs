﻿using PageEditors.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PageEditors.Data.Repository.PageContentsRepository.SupportUsPageRepository
{
    public class SupportUsContentRepository : ISupportUsContentRepository
    {

        List<ResourceContent> resources;
        List<Pair> contentResources;
        public SupportUsContentRepository()
        {
            resources = new List<ResourceContent>()
            {
                new ResourceContent(){ Code = "Espacios", Title="Espacios", ImageUrl="", Description="Espacios para poder realizar actividades de los clubes"},
                new ResourceContent(){ Code = "Visitas", Title="Visitas", ImageUrl="", Description="Visitas o recorridos para aprender sobre el mundo educativo y empresarial"},
                new ResourceContent(){ Code = "Facilitadores", Title="Facilitadores", ImageUrl="", Description="Instruir a las nuevas generaciones para que puedan iniciarse en el mundo real"}
            };
            contentResources = new List<Pair>()
            {
                new Pair(){ Code= "Espacios", Resource="Area" },
                new Pair(){ Code= "Espacios", Resource="Sala de Reuniones" },
                new Pair(){ Code= "Espacios", Resource="Auditorio" },
                new Pair(){ Code= "Visitas", Resource="Colegio para ir a visitar" },
                new Pair(){ Code= "Visitas", Resource="Visitas guiadas a empresas" },
                new Pair(){ Code= "Facilitadores", Resource="Persona que tenga ganas de apoyar en los clubes" }
             };
        }

        public Pair addPairToResource(Pair pair)
        {
            contentResources.Add(pair);
            return pair;
        }

        public bool deletePair(string code, string resource)
        {
            Pair pair = contentResources.SingleOrDefault(p => p.Code == code && p.Resource == resource);
            return contentResources.Remove(pair);
        }

        public IEnumerable<ResourceContent> getAllResources()
        {
           foreach(ResourceContent r in resources)
            {
                r.Resources = contentResources.Where(c => c.Code == r.Code);
            }
            return resources;
        }

        public Pair getPair(string code, string resource)
        {
            return contentResources.SingleOrDefault(c => c.Code == code && c.Resource == resource);
        }

        public ResourceContent getSpecificResource(string code)
        {
            var resource = resources.SingleOrDefault(c => c.Code == code);
            resource.Resources = contentResources.Where(c => c.Code == code);
            return resource;
        }

        public Pair updatePairOfResource(string code, string resource, Pair pair)
        {
            var pairToUpdate = contentResources.SingleOrDefault(p => p.Code == code && p.Resource == resource);
            pairToUpdate.Resource = pair.Resource;
            return pairToUpdate;
        }

        public ResourceContent updateResource(string code, ResourceContent resource)
        {
            var resourceToUpdate = resources.SingleOrDefault(c => c.Code == code);
            resourceToUpdate.Title = resource.Title;
            resourceToUpdate.Description = resourceToUpdate.Description;
            resourceToUpdate.ImageUrl = resource.ImageUrl;
            return resourceToUpdate;
        }
    }
}
